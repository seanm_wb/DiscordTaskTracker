# DiscordTaskTrackerBot
This bot will get a task from a user with a reminder time, and remind the user in a private message when the time has been reached

## Example
An example command:

```
!r 8 hours !t fill out your time card
```